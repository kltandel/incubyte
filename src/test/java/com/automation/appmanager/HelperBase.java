package com.automation.appmanager;

import static com.automation.TestRunner.TestRunner.currentDIR;
import static com.automation.TestRunner.TestRunner.driver;
import java.util.concurrent.TimeUnit;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class HelperBase {
	
	static Actions actions;
	public static String methodName;
	public static final String ACCOUNT_SID = "ACea06c973726ed34196f7f640d733c9cd";
	public static final String AUTH_TOKEN = "548261292bfb32329fbebeeb4bb54663";
	static String excelFilePath = currentDIR + "\\src\\test\\resources\\GrowExxAutomation.xlsx";
	Workbook wb1 = new XSSFWorkbook();
	
	/**
	 * Launch browser
	 */
	public static void openBrowser(String browser) throws Exception {
		
		try {
			
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver",
						currentDIR + "//src//test//resources//Drivers//chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						currentDIR + "//src//test//resources//Drivers//geckodriver.exe");
			}

			int implicitWaitTime = (20);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			System.out.println("Exception occured in starting browser " + e);
			
		}
		Thread.sleep(1000);
	}

	/**
	 * Open application URL
	 */
	public static void openURL(String url) {
		
		driver.get(url);
		
	}

	/**
	 * Click on web element
	 */
	public static void clickByXpath(String xpath) throws InterruptedException {
		
		driver.findElement(By.xpath(xpath)).click();
		
	}

	/**
	 * Enter text value in web element
	 */
	public static void enterText(String xpath, String textToEnter) {
		
		driver.findElement(By.xpath(xpath)).clear();
		driver.findElement(By.xpath(xpath)).sendKeys(textToEnter);
		
	}

	/**
	 * Wait for seconds
	 */
	public static void waitFor(int milliSeconds) throws Exception, InterruptedException {
		
		Thread.sleep((milliSeconds) * 1000);
		
	}


	/**
	 * Check element is displayed or not
	 */
	public static Boolean isElementDisplayed(String xpath) {
		
		return driver.findElement(By.xpath(xpath)).isDisplayed();
		
	}


}