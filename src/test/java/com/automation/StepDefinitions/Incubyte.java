package com.automation.StepDefinitions;

import static com.automation.TestRunner.TestRunner.driver;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.By;
import org.testng.asserts.SoftAssert;
import com.automation.appmanager.HelperBase;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Incubyte extends HelperBase {

	public static Properties LocalProperty, PageLocators;
	public FileInputStream fs, fsLocators;
	public static String currentDIR = System.getProperty("user.dir");
	boolean idElementPresent;

	@Given("User open the URL")
	public void user_open_the_url() throws IOException {

		LocalProperty = new Properties();
		PageLocators = new Properties();
		fs = new FileInputStream(currentDIR + "\\src\\test\\resources\\local.properties");
		fsLocators = new FileInputStream(currentDIR + "\\src\\test\\resources\\PageLocators\\GmailLocators.properties");
		LocalProperty.load(fs);
		PageLocators.load(fsLocators);
		openURL(LocalProperty.getProperty("webUrl"));

	}
	

	@When("User enter valid credentials and click on next button {string}")
	public void user_enter_valid_credentials_and_click_on_next_button(String emailId) throws Exception {

		enterText(PageLocators.getProperty("emailTxt"), emailId);
		waitFor(3);
		clickByXpath(PageLocators.getProperty("nextBtn"));
		waitFor(3);
		
		enterText(PageLocators.getProperty("passwordTxt"), LocalProperty.getProperty("Password"));
		waitFor(3);
		clickByXpath(PageLocators.getProperty("nextBtn"));

	}
	

	@Then("User should be logged in successfully")
	public void user_should_be_logged_in_successfully() {


		SoftAssert softAssertion = new SoftAssert();
		String currentURL = driver.getCurrentUrl();
		softAssertion.assertEquals(currentURL, "https://mail.google.com/mail/u/0/#inbox" );
		softAssertion.assertAll();
	}

	@Given("User is on inbox page")
	public void user_is_on_inbox_page() {
			driver.navigate().to("https://mail.google.com/mail/u/0/#inbox");
	}

	@When("User click on compose button")
	public void user_click_on_compose_button() throws InterruptedException, Exception {
		
		waitFor(5);
		clickByXpath(PageLocators.getProperty("composeBtn"));
	}

	@When("enter valid data and click on send button {string} {string} {string}")
	public void enter_valid_data_and_click_on_send_button(String emailId, String subject, String body) throws Exception {
	    
		enterText((PageLocators.getProperty("toEmailTxt")), emailId);
		enterText((PageLocators.getProperty("subjectTxt")), subject);
		enterText((PageLocators.getProperty("bodyTxt")), body);
		clickByXpath(PageLocators.getProperty("sendBtn"));
		waitFor(5);
		
	}
	

	@Then("email should be sent successfully")
	public void email_should_be_sent_successfully() {
		
	    driver.findElement(By.linkText("sent")).click();
	    Boolean Display = isElementDisplayed(PageLocators.getProperty("IncubyteLocator"));
	    SoftAssert softAssertion = new SoftAssert();
		softAssertion.assertTrue(Display);
		softAssertion.assertAll();
		
	}
		
}