package com.automation.TestRunner;

import java.io.FileInputStream;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.automation.appmanager.HelperBase;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = { "src/test/resources/features/incubyte.feature" }, glue = {
		"com.automation.StepDefinitions" }, plugin = { "pretty", "pretty:test-output/Pretty-report/cucumber-pretty.txt",
				"json:target/cucumber-reports/report.json" }, monochrome = true, tags = "@run")

public class TestRunner extends AbstractTestNGCucumberTests {

	public FileInputStream fs;
	public static Properties LocalProperty;
	public static WebDriver driver;
	public static String currentDIR = System.getProperty("user.dir");

	/**
	 * Before Suite Annotation It will be executed only once
	 */
	@BeforeSuite
	public void setUP() throws Exception {

		LocalProperty = new Properties();
		fs = new FileInputStream(currentDIR + "\\src\\test\\resources\\local.properties");
		LocalProperty.load(fs);
		HelperBase.openBrowser(LocalProperty.getProperty("browser"));
		Thread.sleep(3000);

	}

	/**
	 * After Suite Annotation It will be executed at the end of execution
	 */
	@AfterSuite
	public void tearDown() throws Exception {
		
		Thread.sleep(2000);
		driver.quit();

	}
}