Feature: Gmail send email functionality test
  User want to test the functionality of gmail application

	@run @smoke
    Scenario Outline: Validate login functionality with valid data
    Given User open the URL
    When User enter valid credentials and click on next button "<EmailId>"
    Then User should be logged in successfully

    Examples: 
      | EmailId 							|
      |	ketantandel.qa@gmail.com	 		|
      

    @run @smoke
    Scenario Outline: Validate send email functionality with valid data
    Given User is on inbox page
    When User click on compose button
    And enter valid data and click on send button "<RecipientEmailId>" "<Subject>" "<Body>"
    Then email should be sent successfully

    Examples: 
      | RecipientEmailId 					| Subject			| Body 		                    	|
      |	ketantandel.qa@gmail.com	 		| Incubyte			| Automation QA test for Incubyte 	|
      
      
   